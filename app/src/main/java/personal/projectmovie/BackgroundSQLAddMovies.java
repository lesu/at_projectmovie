package personal.projectmovie;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;

import personal.projectmovie.DB.MovieDB;


class BackgroundSQLAddMovies extends AsyncTask<Void, Void, Void>{

    @Override
    protected Void doInBackground(Void... params) {
        MovieDB movieDB = new MovieDB(MainActivity.currentMainActivity);

        for (int i = 0; i < MoviesHost.movies.size(); i++) {
            movieDB.addMovie(MoviesHost.movies.get(i));
        }

        return null;
    }


    @Override
    protected void onPostExecute(Void aVoid) {
        Toaster.showToast(MainActivity.currentMainActivity,"Movie list saved locally",false);
    }
}
