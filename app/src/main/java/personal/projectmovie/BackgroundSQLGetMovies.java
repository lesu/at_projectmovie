package personal.projectmovie;

import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.Arrays;

import personal.projectmovie.DB.MovieDB;

class BackgroundSQLGetMovies extends AsyncTask<Void,Void,ArrayList<Movie>> {


    @Override
    protected ArrayList<Movie> doInBackground(Void... params) {

        MovieDB movieDB = new MovieDB(MainActivity.currentMainActivity);
        return new ArrayList<Movie>(Arrays.asList(movieDB.getAllMovies()));
    }


    @Override
    protected void onPostExecute(ArrayList<Movie> movies) {
        Toaster.showToast(MainActivity.currentMainActivity,"Finished local loading",false);
        MoviesHost.movies = movies;
        MoviesHost.moviesSet = true;
        MoviesHost.online = false;
        MainActivity.currentMainActivity.populateMovieView();

    }
}
