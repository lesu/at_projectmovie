package personal.projectmovie;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {
    static MainActivity currentMainActivity;
    private Button buttonRefresh;
    private Button buttonToggleView;
    private ListView listViewMovies;
    private GridView gridViewMovies;
    private boolean viewAsList = true;

    static final String INTENT_MOVIE_KEY = "movie";
    private static final String JSON_URL = "http://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key=18147b0826078c6d5e462bf97f3e032d";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.init();
    }

    private void init() {
        MainActivity.currentMainActivity = this;
        this.buttonRefresh = (Button) this.findViewById(R.id.main_button_refresh);
        this.buttonToggleView = (Button) this.findViewById(R.id.main_button_toggle_view);
        this.listViewMovies = (ListView) this.findViewById(R.id.main_list_view);
        this.gridViewMovies = (GridView) this.findViewById(R.id.main_grid_view);
        this.listViewMovies.setVisibility(View.INVISIBLE);
        this.gridViewMovies.setVisibility(View.INVISIBLE);

        this.buttonToggleView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.this.viewAsList = !MainActivity.this.viewAsList;
                MainActivity.this.populateMovieView();
            }
        });
        this.listViewMovies.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MainActivity.this.startActivity(new Intent(MainActivity.this, DetailsActivity.class).putExtra(MainActivity.INTENT_MOVIE_KEY, MoviesHost.movies.get(position)));
            }
        });
        this.gridViewMovies.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MainActivity.this.startActivity(new Intent(MainActivity.this, DetailsActivity.class).putExtra(MainActivity.INTENT_MOVIE_KEY,MoviesHost.movies.get(position)));
            }
        });
        this.buttonRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.this.loadMovieListFromJSON();
            }
        });
        this.loadMovieListFromSQLite();
        Toaster.showToast(this, "Done", false);

    }

    private void loadMovieListFromJSON() {
//        Toast.makeText(MainActivity.this,"Loading movie list, please wait...",Toast.LENGTH_SHORT).show();
        Toaster.showToast(MainActivity.this, "Loading movie list, please wait...", false);
        new BackgroundJSONParsing().execute(MainActivity.JSON_URL);
    }

    void populateMovieView() {
        if(this.viewAsList) {
            this.listViewMovies.setVisibility(View.VISIBLE);
            this.gridViewMovies.setVisibility(View.INVISIBLE);
            ArrayList<String> movieTitles = new ArrayList<String>();
            for (int i = 0; i < MoviesHost.movies.size(); i++) {
                movieTitles.add(MoviesHost.movies.get(i).getTitle());
            }
            this.listViewMovies.setAdapter(new ArrayAdapter(this, R.layout.row, movieTitles));
        } else {
            this.listViewMovies.setVisibility(View.INVISIBLE);
            this.gridViewMovies.setVisibility(View.VISIBLE);
            GridViewImageAdapter gridViewImageAdapter = new GridViewImageAdapter(this, MoviesHost.movies.toArray(new Movie[0]));
            this.gridViewMovies.setAdapter(gridViewImageAdapter);
        }
    }

    private void loadMovieListFromSQLite() {
        Toaster.showToast(MainActivity.currentMainActivity,"Started local loading",false);
        new BackgroundSQLGetMovies().execute();
    }
}
