package personal.projectmovie;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

class BackgroundJSONParsing extends AsyncTask<String,Movie,ArrayList<Movie>>{
    private AppCompatActivity uiActivity = MainActivity.currentMainActivity;
    private ListView listView;
    private Button buttonRefresh;
    private boolean uiInitialized = false;


    @Override
    protected ArrayList<Movie> doInBackground(String... urls) {
        ArrayList<Movie> movies = new ArrayList<Movie>();
        try {
            JSONObject jsonObject = this.getJSONObject(urls[0]);
            JSONArray results = jsonObject.getJSONArray("results");
            for (int i = 0; i < results.length(); i++) {
                movies.add(this.getMovie(results.getJSONObject(i)));
            }
        } catch (JSONException e) {
            Log.e("JSONException", e.getMessage(), e);
//            Toast.makeText(this.uiActivity, e.getMessage(), Toast.LENGTH_SHORT).show();
            Toaster.showToast(this.uiActivity,e.getMessage(),false);
        } catch (IOException e) {
            Log.e("IOException", e.getMessage(), e);
//            Toast.makeText(this.uiActivity, e.getMessage(), Toast.LENGTH_SHORT).show();
            Toaster.showToast(this.uiActivity,e.getMessage(),false);
        }
        return movies;
    }


    @Override
    protected void onPostExecute(ArrayList<Movie> movies) {
        MoviesHost.movies = movies;
        MoviesHost.moviesSet = true;
        MoviesHost.online = true;
        MainActivity.currentMainActivity.populateMovieView();
        new BackgroundSQLAddMovies().execute();
    }


    @Override
    protected void onProgressUpdate(Movie... movies) {
        for (int i = 0; i < movies.length; i++) {
            MoviesHost.movies.add(movies[i]);
        }
        MainActivity.currentMainActivity.populateMovieView();
    }

    private Movie getMovie(JSONObject jsonObject) throws JSONException {
        long tmdbId = Long.parseLong(jsonObject.getString("id"));
        String title = jsonObject.getString("original_title");
        String imageUrl = "http://image.tmdb.org/t/p/w185/"+jsonObject.getString("poster_path");
        String overview = jsonObject.getString("overview");

        Date releaseDate;
        {
            String[] dateData = jsonObject.getString("release_date").split("-");
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR,Integer.parseInt(dateData[0]));
            calendar.set(Calendar.MONTH,Integer.parseInt(dateData[1]));
            calendar.set(Calendar.DAY_OF_MONTH,Integer.parseInt(dateData[2]));
            releaseDate = calendar.getTime();
        }
        float voteAverage = Float.parseFloat(jsonObject.getString("vote_average"));
        return new Movie(tmdbId,title,imageUrl,overview,releaseDate,voteAverage);
    }

    private JSONObject getJSONObject(String urlString) throws IOException, JSONException {
        StringBuffer stringBuffer = new StringBuffer();
        URL url = new URL(urlString);
        HttpURLConnection httpURLConnection = null;
        try {
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.connect();
            BufferedInputStream bufferedInputStream = new BufferedInputStream(httpURLConnection.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(bufferedInputStream));

            String line;
            while ((line = bufferedReader.readLine())!=null) {
                stringBuffer.append(line);
            }

            return new JSONObject(stringBuffer.toString());
        } finally {
            httpURLConnection.disconnect();
        }
    }



}
