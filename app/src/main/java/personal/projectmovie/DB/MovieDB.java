package personal.projectmovie.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.ParseException;
import java.util.ArrayList;

import personal.projectmovie.Movie;


public final class MovieDB extends SQLiteOpenHelper{
    private static final int LAST_DB_VERSION = MetaV2.DB_VERSION;
    private static final String LAST_DB_NAME = MetaV2.DB_NAME;

    public MovieDB(Context context) {
        super(context, LAST_DB_NAME,null, MovieDB.LAST_DB_VERSION);
    }

    /**
     * Called when the database is created for the first time. This is where the
     * creation of tables and the initial population of the tables should happen.
     *
     * @param sqLiteDatabase The database.
     */
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        this.createMovieTable(sqLiteDatabase);
    }

    /**
     * Called when the database needs to be upgraded. The implementation
     * should use this method to drop tables, add tables, or do anything else it
     * needs to upgrade to the new schema version.
     * <p/>
     * <p>
     * The SQLite ALTER TABLE documentation can be found
     * <a href="http://sqlite.org/lang_altertable.html">here</a>. If you add new columns
     * you can use ALTER TABLE to insert them into a live table. If you rename or remove columns
     * you can use ALTER TABLE to rename the old table, then create the new table and then
     * populate the new table with the contents of the old table.
     * </p><p>
     * This method executes within a transaction.  If an exception is thrown, all changes
     * will automatically be rolled back.
     * </p>
     *
     * @param db         The database.
     * @param oldVersion The old database version.
     * @param newVersion The new database version.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + "["+MetaV1.MOVIE_TABLE_NAME+"]");
        createMovieTableV2(db);
    }



//    Note Specific methods and constants.

    class MetaV1 {
        final static String DB_NAME = "MoviesDB";
        final static int DB_VERSION = 1;
        final static String MOVIE_TABLE_NAME = "movie";
        final static String MOVIE_TABLE_COL_1_ID_NAME = "id";
        final static String MOVIE_TABLE_COL_2_TITLE_NAME = "title";
        final static String MOVIE_TABLE_COL_3_OVERVIEW_NAME = "overview";
        final static String MOVIE_TABLE_COL_4_RELEASE_DATE_NAME = "release date";
        final static String MOVIE_TABLE_COL_5_AVERAGE_RATING_NAME = "average rating";
    }

    class MetaV2 {
        final static String DB_NAME = "MoviesDB";
        final static int DB_VERSION = 2;
        final static String MOVIE_TABLE_NAME = "movie";
        final static String MOVIE_TABLE_COL_1_TMDBID_NAME = "tmdbId";
        final static String MOVIE_TABLE_COL_2_TITLE_NAME = "title";
        final static String MOVIE_TABLE_COL_3_IMAGE_URL_NAME = "image url";
        final static String MOVIE_TABLE_COL_4_OVERVIEW_NAME = "overview";
        final static String MOVIE_TABLE_COL_5_RELEASE_DATE_NAME = "release date";
        final static String MOVIE_TABLE_COL_6_AVERAGE_RATING_NAME = "average rating";
    }

    private boolean createMovieTable(SQLiteDatabase sqLiteDatabase) {
        Table movies = new Table(MetaV1.MOVIE_TABLE_NAME);
        Column id = new Column(MetaV1.MOVIE_TABLE_COL_1_ID_NAME,ColumnType.INTEGER);
        id.addAttribute(ColumnAttribute.PRIMARY_KEY.getName());
        id.addAttribute(ColumnAttribute.AUTOINCREMENT.getName());
        movies.addComlumn(id);
        movies.addComlumn(new Column(MetaV1.MOVIE_TABLE_COL_2_TITLE_NAME, ColumnType.TEXT));
        movies.addComlumn(new Column(MetaV1.MOVIE_TABLE_COL_3_OVERVIEW_NAME, ColumnType.TEXT));
        movies.addComlumn(new Column(MetaV1.MOVIE_TABLE_COL_4_RELEASE_DATE_NAME, ColumnType.TEXT));
        movies.addComlumn(new Column(MetaV1.MOVIE_TABLE_COL_5_AVERAGE_RATING_NAME, ColumnType.REAL));

        String createTableString = this.createTable(movies);
        sqLiteDatabase.execSQL(createTableString);
        return true;
    }

    private boolean createMovieTableV2(SQLiteDatabase sqLiteDatabase) {
        Table movies = new Table(MetaV2.MOVIE_TABLE_NAME);
        Column id = new Column(MetaV2.MOVIE_TABLE_COL_1_TMDBID_NAME,ColumnType.INTEGER);
        id.addAttribute(ColumnAttribute.PRIMARY_KEY.getName());
//        id.addAttribute(ColumnAttribute.AUTOINCREMENT.getName());
        movies.addComlumn(id);
        movies.addComlumn(new Column(MetaV2.MOVIE_TABLE_COL_2_TITLE_NAME, ColumnType.TEXT));
        movies.addComlumn(new Column(MetaV2.MOVIE_TABLE_COL_3_IMAGE_URL_NAME, ColumnType.TEXT));
        movies.addComlumn(new Column(MetaV2.MOVIE_TABLE_COL_4_OVERVIEW_NAME, ColumnType.TEXT));
        movies.addComlumn(new Column(MetaV2.MOVIE_TABLE_COL_5_RELEASE_DATE_NAME, ColumnType.TEXT));
        movies.addComlumn(new Column(MetaV2.MOVIE_TABLE_COL_6_AVERAGE_RATING_NAME, ColumnType.REAL));

        String createTableString = this.createTable(movies);
        sqLiteDatabase.execSQL(createTableString);
        return true;
    }

    public long addMovie(Movie movie) {
        SQLiteDatabase writableDatabase = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("["+ MetaV2.MOVIE_TABLE_COL_1_TMDBID_NAME+"]", movie.getTmdbId());
        contentValues.put("["+ MetaV2.MOVIE_TABLE_COL_2_TITLE_NAME+"]", movie.getTitle());
        contentValues.put("["+ MetaV2.MOVIE_TABLE_COL_3_IMAGE_URL_NAME+"]", movie.getImageUrl());
        contentValues.put("["+ MetaV2.MOVIE_TABLE_COL_4_OVERVIEW_NAME +"]", movie.getOverview());
        contentValues.put("["+ MetaV2.MOVIE_TABLE_COL_5_RELEASE_DATE_NAME +"]", movie.getReleaseDateAsString());
        contentValues.put("["+ MetaV2.MOVIE_TABLE_COL_6_AVERAGE_RATING_NAME +"]", movie.getAverageRating());

        long id = writableDatabase.insert(MetaV2.MOVIE_TABLE_NAME, null, contentValues);

        writableDatabase.close();
        return id;
    }

    public int deleteMovie(Movie movie) {
        return this.deleteMovie(movie.getTmdbId());
    }

    public int deleteMovie(long movieId) {
        SQLiteDatabase writableDatabase = this.getWritableDatabase();
        int numberOfDeletedRows = writableDatabase.delete(MetaV2.MOVIE_TABLE_NAME, MetaV2.MOVIE_TABLE_COL_1_TMDBID_NAME + "=" + movieId, null);
        writableDatabase.close();
        return numberOfDeletedRows;
    }

    public Movie[] getAllMovies() {
        ArrayList<Movie> movies = new ArrayList<Movie>();
        SQLiteDatabase readableDatabase = this.getReadableDatabase();
        Cursor cursor = readableDatabase.query(MetaV2.MOVIE_TABLE_NAME, null, null, null, null, null, null);

        if(cursor.moveToFirst()) {
            do {
                Movie movie = new Movie(cursor.getInt(0), cursor.getString(1));
                movie.setImageUrl(cursor.getString(2));
                movie.setOverview(cursor.getString(3));
                try {
                    movie.setReleaseDate(Movie.getDateFromString(cursor.getString(4)));
                } catch (ParseException e) {
                    Log.e("ParseException",e.getMessage(),e);
                }
                movie.setAverageRating(cursor.getFloat(5));
                movies.add(movie);
            }
            while (cursor.moveToNext());
        }
        readableDatabase.close();
        return movies.toArray(new Movie[0]);
    }

    public Movie getMovie(int movieId) {
        SQLiteDatabase readableDatabase = this.getReadableDatabase();
        Cursor cursor = readableDatabase.query(MetaV2.MOVIE_TABLE_NAME, null, MetaV2.MOVIE_TABLE_COL_1_TMDBID_NAME +"="+movieId, null, null, null, null);
        cursor.moveToFirst();
        Movie movie = new Movie(cursor.getInt(0), cursor.getString(1));
        movie.setImageUrl(cursor.getString(2));
        movie.setOverview(cursor.getString(3));
        try {
            movie.setReleaseDate(Movie.getDateFromString(cursor.getString(4)));
        } catch (ParseException e) {
            Log.e("ParseException",e.getMessage(),e);
        }
        movie.setAverageRating(cursor.getFloat(5));
        readableDatabase.close();
        return movie;
    }

//    Generalized Methods
    private String createTable (Table table) {

        StringBuffer result = new StringBuffer();
        Column[] columns = table.getColumns();

        result.append("CREATE TABLE [");
        result.append(table.getName());
        result.append("] (");

        for (int i = 0; i < columns.length; i++) {
            result.append("["+columns[i].getName()+"]");
            result.append(" ");
            result.append(columns[i].getType());
            result.append(" ");
            {
                String[] attributes = columns[i].getAttributes();
                for (int j = 0; j < attributes.length; j++) {
                    result.append(attributes[j]);
                    if(j!=(attributes.length-1)){
                        result.append(" ");
                    }

                }
            }
            if(i!=(columns.length-1)){
                result.append(",");
            }
        }

        result.append(")");
        return result.toString();
    }
}
