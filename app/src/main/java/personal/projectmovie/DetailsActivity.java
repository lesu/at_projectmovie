package personal.projectmovie;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.Date;

public class DetailsActivity extends AppCompatActivity {
    private TextView textViewId;
    private RatingBar ratingBar;
    private TextView textViewTitle;
    private TextView textViewDate;
    private ImageView imageView;
    private TextView textViewOverview;
    private Intent sourceIntent;
    private Movie movie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        this.init();
    }

    private void init() {
        this.textViewId = (TextView) this.findViewById(R.id.details_text_view_id);
        this.ratingBar = (RatingBar) this.findViewById(R.id.details_rating_bar);
        this.textViewTitle = (TextView) this.findViewById(R.id.details_text_view_title);
        this.textViewDate = (TextView) this.findViewById(R.id.details_text_view_date);
        this.imageView = (ImageView) this.findViewById(R.id.details_image_view);
        this.textViewOverview = (TextView) this.findViewById(R.id.details_text_view_overview);
        this.sourceIntent = this.getIntent();
        this.movie = (Movie) this.sourceIntent.getSerializableExtra(MainActivity.INTENT_MOVIE_KEY);

        this.textViewId.setText("Id : "+this.movie.getTmdbId());
        this.ratingBar.setRating(this.movie.getAverageRating());
        this.textViewTitle.setText(this.movie.getTitle());
        this.textViewDate.setText("Release date : "+this.movie.getReleaseDateAsString());
        Picasso.with(this).load(this.movie.getImageUrl()).into(this.imageView);
        this.textViewOverview.setText(this.movie.getOverview());
    }

}
